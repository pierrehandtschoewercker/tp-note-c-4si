# TP NOTE C 4SI

bouts de code pour nous aider pendant les partiel 

## SHM

Une SHM est une mémoire partagée entre plusieurs process.


```c
#include "pthreadshm.h"

key_t getShmKey()
{
    return ftok(".", "x");
}

int getShm(key_t shmKey)
{
    return shmget(shmKey, sizeof(Pthreadshm), IPC_CREAT | 0666);
}

Pthreadshm *getPthreadShm(int shmId)
{
    Pthreadshm *shmPtr = (Pthreadshm *)shmat(shmId, NULL, 0);
    if ((int)shmPtr == -1)
    {
        printf("*** shmat error (server) ***\n");
        exit(1);
    }
    return shmPtr;

}
```



Le code ci-dessus sert à créer et s'attacher à une SHM.
**Remarquer le Cast en Pthreadshm très important pour pouvoir utilisé la structure contenu dans la SHM** (Vous casterez avec e nom de la structure que vous aurez défini).


```c
#ifndef pthreadshm_h

typedef struct pthreadshm
{
    int dataSHm1;
    char dataSHM2[30];
} Pthreadshm;

key_t getShmKey();
int getShm(key_t shmKey);
Pthreadshm *getPthreadShm(int shmId);

#endif
```


Le code ci-dessus défini la structure des données que l'on peut trouver dans la SHM. Libre à vous d'ajouter d'autre données de tout type.


```c
//attacher la shm
    key_t shmKey;
    int shmId;
    Pthreadshm *shmPtr;

    shmKey = getShmKey();
    shmId = getShm(shmKey);
    if (shmId < 0)
    {
        printf("*** shmget error (server) ***\n");
        exit(1);
    }

    shmPtr = getPthreadShm(shmId);
```


Ceci vous permettra d'attacher la SHM dans le programme C qui doit y accéder.

Le code suivant vous montrera comment accéder et modifier les données de la SHM : 

```c
    shmPtr->dataSHm1--; //décrémenter de  1 la donnée contenue dans le SHM
    strncpy(packet.account.password, shmPtr->dataSHM2, 30); copie d'une string dans la SHM;
```


Enfin vous pourvez détacher une SHM avec le code suivant 

`shmdt(shmPtr);`
`shmctl(shmId,IPC_RMID, NULL); //permet de détruire la SHM`

**A noter que les process spawné avec fork() hérite des SHM attachées au parent. Attention la SHM doit-être attachée avant le fork().**.

**Pour vous aider à debugger sous linux `ipcs -m` affiche les SHM présente sur le système**

## Sémaphores

- Types de  Sémaphores

- Exclusion mutuelle
- Sémaphores de partagée
- Sémaphores privées

### Exclusion mutuelle

Le compteur de la sémaphore est a 1. Le compteur sert à définir si la section critique peut être exécutée par un process.
Si le compteur >= 0 la section critique ne peut ps être éxécutée.

Création sémaphore 

```c
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/sem.h>

#define CLE 100

//Union pour initialiser le semaphoe
union semun
{
    int val;                //Valeur pour SETVAL (Compteur)
    struct semid_ds* buf;   //Buffer pour IPC_STAT, IPC_SET
    unsigned short* array;  //Table pour GETALL, SETALL
    struct seminfo* __buff; //Buffer pour IPC_INFO
};

int main(int argc, char* argv[])
{
    union semun sn; // "structure" qui contient les attribut de la sémaphore

    //Structure pour faire des P et V
    struct sembuf sbuf = {0,0,0};


    //crée le semaphore
    int idSem = semget((key_t)CLE, 1, IPC_CREAT|0700); // création de la sémaphore

    //Initialise le compteur
    sn.val = 1; // ceci correspond au compteur (ici sémaphore privée car le compteur est initialisé à zéro).
    semctl(idSem, 0, SETVAL, sn); // met à jour la sémaphore.

    //ret = valeur du compteur
    int ret = semctl(id, 0, GETVAL, 0);

    // P de sémaphore
    sbuf.sem_num = 0;
    sbuf.sem_op = -1;
    sbuf.sem_flg = 0;
    semop(idSem, &sbuf, 1);

    //V(sem)
    sbuf.sem_num = 0;
    sbuf.sem_op = +1;
    sbuf.sem_flg = 0;
    semop(idSem, &sbuf, 1);

    return 0;
}
```

### Exemple den Programme qui utilise des sem

```c


```

## fork

```c
int main(void){
    int fils = fork();

    if(fils == - 1){
        //echec du fork
        exit();
    }else if(!fils){
        //code du fils
    }else{
        //code du père
        int status; //res du fils
        wait(&status) // le père récupère le code d'erreur ou succès du fils
    }
}
```



## Sockets

```c
    int sock = socket(AF_INET, SOCK_STREAM, 0); //ipv4 et TCP
    struct sockaddr_in addr;
    int addr_size;

    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY); // on écoute toute les addr
    addr.sin_port = htons(3000);              // def du port d'écoute
    addr_size = sizeof(addr);

    bind(sock, (const struct sockaddr *)&addr, addr_size); // bind socket to adresse and port
    listen(sock, 0);                                       // écoute à l'infini pour les connnexions
    *client_socket = accept(sock, (struct sockaddr *)&addr, (socklen_t *)&addr_size); // accepter une connexion sur la socket
```





